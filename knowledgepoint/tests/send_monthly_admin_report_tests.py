from django.core.management.base import CommandError
from django.contrib.auth.models import Group
from django.test import TestCase

from django_dynamic_fixture import N
from mock import Mock, patch, MagicMock, PropertyMock

from ..management.commands.send_monthly_admin_report import (
    ANSWERS_AND_COMMENTS_BY_GROUP_NAME,
    ANSWERED_QUESTIONS_FROM_PUBLIC_DOMAIN_NAME,
    Command,
    MEMBERS_BY_COUNTRY_NAME,
    MEMBERS_BY_TAG_OF_INTEREST_NAME,
    MEMBERS_TAG_NAME,
    QUESTIONS_BY_COUNTRY_NAME,
    QUESTIONS_BY_TAG_OF_INTEREST_NAME,
    QUESTIONS_FROM_GROUP_NAME,
    QUESTIONS_MOVED_TO_PUBLIC_DOMAIN_NAME,
    UNANSWERED_QUESTIONS_BY_GROUP_NAME,
    format_report_tag
)

from ..reports import (
    AnswersAndCommentsByGroupReport,
    GroupMembershipReport,
    MembersInterestedInAreaReport,
    QuestionsAnsweredFromOtherSitesReport,
    QuestionsAskedByGroupReport,
    QuestionsMovedToPublicDomainReport,
    UnansweredQuestionsByGroupReport,
)


class SendMonthlyAdminReportTest(TestCase):

    def test_get_report_context_contains_group_numbers(self):
        group_list = [N(Group, name='ABC')]
        tag_name = format_report_tag(MEMBERS_TAG_NAME, group_list[0])

        mock_command = Mock(spec=Command, add_report_data=Command().add_report_data)
        type(mock_command).groups_list = PropertyMock(return_value=group_list)
        expected_data = {GroupMembershipReport.report_label: 0}
        actual_data = Command.get_report_context_data(mock_command, group_list[0])['group_reports']['ABC'][tag_name]
        self.assertDictContainsSubset(expected_data, actual_data)

    def test_method_to_add_report_data_calls_report_generate_data(self):
        command = Command()
        report_klass = MagicMock
        report_klass.generate_data = Mock(return_value={})

        group_mock = Mock()
        mock_name = PropertyMock(return_value='mock_name')
        type(group_mock).name = mock_name
        tag_name = format_report_tag(MEMBERS_TAG_NAME, group_mock)

        group_data = {group_mock.name: {tag_name: {}}}

        command.add_report_data(group_mock, group_data, tag_name, report_klass)

        self.assertTrue(report_klass.generate_data.called)

    @patch('knowledgepoint.management.commands.send_monthly_admin_report.MembersInterestedInAreaReport.model')
    def test_get_report_context_contains_area_of_intrest_numbers(self, tags):
        group_list = [N(Group, name='ABC')]
        tag_name = format_report_tag(MEMBERS_BY_TAG_OF_INTEREST_NAME, group_list[0])

        annotated_data = [
            {'tag__name': 'Waste', 'tag__name__count': 10},
            {'tag__name': 'Agriculture', 'tag__name__count': 20}
        ]
        annotate = Mock(return_value=annotated_data)
        values = Mock(return_value=Mock(annotate=annotate))
        exclude = Mock(return_value=Mock(values=values))
        tags.objects = Mock(filter=Mock(return_value=Mock(exclude=exclude)))

        mock_command = Mock(spec=Command, add_report_data=Command().add_report_data)
        type(mock_command).groups_list = PropertyMock(return_value=group_list)

        expected_data = {'Agriculture': 20}
        actual_data = Command.get_report_context_data(mock_command, group_list[0])['group_reports']['ABC'][tag_name]

        self.assertDictContainsSubset(expected_data, actual_data)

    @patch('knowledgepoint.management.commands.send_monthly_admin_report.MembersInCountryReport.model')
    def test_get_report_context_contains_country_numbers(self, countries):
        group_list = [N(Group, name='ABC')]
        tag_name = format_report_tag(MEMBERS_BY_COUNTRY_NAME, group_list[0])

        annotated_data = [
            {'user__country': 'AF', 'user__country__count': 5},
            {'user__country': 'GB', 'user__country__count': 10},
            {'user__country': '', 'user__country__count': 1}
        ]
        annotate = Mock(return_value=annotated_data)
        values = Mock(return_value=Mock(annotate=annotate))
        exclude = Mock(return_value=Mock(values=values))
        countries.objects = Mock(filter=Mock(return_value=Mock(exclude=exclude)))

        mock_command = Mock(spec=Command, add_report_data=Command().add_report_data)
        type(mock_command).groups_list = PropertyMock(return_value=group_list)

        expected_data = {'Afghanistan': 5, 'United Kingdom': 10, 'Total unspecified': 1}
        actual_data = Command.get_report_context_data(mock_command, group_list[0])['group_reports']['ABC'][tag_name]

        self.assertDictContainsSubset(expected_data, actual_data)

    def test_command_raisies_error_when_called_without_groups_option(self):
        command = Command()
        self.assertRaises(CommandError, command.handle)

    @patch('knowledgepoint.management.commands.send_monthly_admin_report.send_templated_mail', new=Mock())
    def test_command_doesnt_raise_error_when_called_with_groups_option(self):
        command = Command()
        command.get_report_context_data = Mock()
        command.handle(group_names='Pi Caname')

    @patch('knowledgepoint.management.commands.send_monthly_admin_report.send_templated_mail')
    def test_one_email_sent_per_group(self, send_emails):
        EXPECTED_EMAIL_COUNT = 5
        command = Command()
        command.get_report_context_data = Mock()
        type(command).groups_list = PropertyMock(return_value=N(Group, n=EXPECTED_EMAIL_COUNT))
        command.handle(group_names='Pi Caname')

        self.assertEqual(EXPECTED_EMAIL_COUNT, send_emails.call_count)

    @patch('knowledgepoint.management.commands.send_monthly_admin_report.QuestionsAskedByGroupReport.model')
    def test_get_report_context_contains_question_numbers(self, group_threads):
        group_list = [N(Group, name='ABC')]
        group_threads.objects = Mock(filter=Mock(return_value=Mock(count=lambda: 5)))
        tag_name = format_report_tag(QUESTIONS_FROM_GROUP_NAME, group_list[0])

        mock_command = Mock(spec=Command, add_report_data=Command().add_report_data)
        type(mock_command).groups_list = PropertyMock(return_value=group_list)

        expected_data = {QuestionsAskedByGroupReport.report_label: 5}
        actual_data = Command.get_report_context_data(mock_command, group_list[0])['group_reports']['ABC'][tag_name]

        self.assertDictContainsSubset(expected_data, actual_data)

    @patch('knowledgepoint.management.commands.send_monthly_admin_report.QuestionsGroupedByTagReport.model')
    def test_get_report_context_contains_quesiton_numbers_by_tag(self, tags):
        group_list = [N(Group, name='ABC')]
        tag_name = format_report_tag(QUESTIONS_BY_TAG_OF_INTEREST_NAME, group_list[0])

        annotated_data = [
            {'group_threads__tags__name': 'Waste', 'group_threads__tags__name__count': 10},
            {'group_threads__tags__name': 'Agriculture', 'group_threads__tags__name__count': 20}
        ]
        annotate = Mock(return_value=annotated_data)
        values = Mock(return_value=Mock(annotate=annotate))
        a_filter = Mock(return_value=Mock(values=values))
        tags.objects = Mock(filter=a_filter)

        mock_command = Mock(spec=Command, add_report_data=Command().add_report_data)
        type(mock_command).groups_list = PropertyMock(return_value=group_list)

        expected_data = {'Agriculture': 20}
        actual_data = Command.get_report_context_data(mock_command, group_list[0])['group_reports']['ABC'][tag_name]

        self.assertDictContainsSubset(expected_data, actual_data)

    @patch('knowledgepoint.management.commands.send_monthly_admin_report.QuestionsGroupedByCountryReport.model')
    def test_get_report_context_contains_question_country_numbers(self, countries):
        group_list = [N(Group, name='ABC')]
        tag_name = format_report_tag(QUESTIONS_BY_COUNTRY_NAME, group_list[0])

        annotated_data = [
            {'author__country': 'AF', 'author__country__count': 5},
            {'author__country': 'GB', 'author__country__count': 10},
            {'author__country': '', 'author__country__count': 1}
        ]
        annotate = Mock(return_value=annotated_data)
        values = Mock(return_value=Mock(annotate=annotate))
        a_filter = Mock(return_value=Mock(values=values))

        def all_threads():
            return [Mock(posts=Mock(filter=a_filter))]

        def get(**_):
            return Mock(group_threads=Mock(all=all_threads))

        countries.objects = Mock(get=get)

        mock_command = Mock(spec=Command, add_report_data=Command().add_report_data)
        type(mock_command).groups_list = PropertyMock(return_value=group_list)

        expected_data = {'Afghanistan': 5, 'United Kingdom': 10, 'Total unspecified': 1}
        actual_data = Command.get_report_context_data(mock_command, group_list[0])['group_reports']['ABC'][tag_name]

        self.assertDictContainsSubset(expected_data, actual_data)

    @patch('knowledgepoint.management.commands.send_monthly_admin_report.AnswersAndCommentsByGroupReport.model')
    def test_get_report_context_contains_answer_and_comment_numbers(self, answers_and_comments):
        group_list = [N(Group, name='ABC')]
        tag_name = format_report_tag(ANSWERS_AND_COMMENTS_BY_GROUP_NAME, group_list[0])

        def get_post_type_filter(**kwargs):
            if kwargs.get('post_type', '') == 'answer':
                the_filter = Mock(count=lambda: 2)
            elif kwargs.get('post_type', '') == 'comment':
                the_filter = Mock(count=lambda: 3)
            else:
                the_filter = Mock(count=lambda: 5)
            return the_filter

        the_filter = Mock(return_value=Mock(filter=get_post_type_filter))

        answers_and_comments.objects = Mock(filter=the_filter)

        mock_command = Mock(spec=Command, add_report_data=Command().add_report_data)
        type(mock_command).groups_list = PropertyMock(return_value=group_list)

        expected_data = {
            'Number of answers': 2,
            'Number of comments': 3,
            'Number of answers and comments': 5
        }
        actual_data = Command.get_report_context_data(mock_command, group_list[0])['group_reports']['ABC'][tag_name]

        self.assertDictContainsSubset(expected_data, actual_data)

    @patch('knowledgepoint.management.commands.send_monthly_admin_report.UnansweredQuestionsByGroupReport.model')
    def test_get_report_context_contains_unanswered_question_numbers(self, unanswered_questions):
        group_list = [N(Group, name='ABC')]
        tag_name = format_report_tag(UNANSWERED_QUESTIONS_BY_GROUP_NAME, group_list[0])

        def count():
            return 5

        exclude = Mock(return_value=Mock(count=count))
        a_filter = Mock(return_value=Mock(exclude=exclude))

        unanswered_questions.objects = Mock(filter=a_filter)

        mock_command = Mock(spec=Command, add_report_data=Command().add_report_data)
        type(mock_command).groups_list = PropertyMock(return_value=group_list)

        expected_data = {UnansweredQuestionsByGroupReport.report_label: 5}
        actual_data = Command.get_report_context_data(mock_command, group_list[0])['group_reports']['ABC'][tag_name]

        self.assertDictContainsSubset(expected_data, actual_data)

    @patch('knowledgepoint.management.commands.send_monthly_admin_report.QuestionsMovedToPublicDomainReport.model')
    def test_get_report_context_contains_numbers_of_question_moved_to_public_domain(self, questions_transferred):
        group_list = [N(Group, name='ABC')]
        tag_name = format_report_tag(QUESTIONS_MOVED_TO_PUBLIC_DOMAIN_NAME, group_list[0])

        def count():
            return 5

        second_filter = Mock(return_value=Mock(count=count))
        a_filter = Mock(return_value=Mock(filter=second_filter))

        questions_transferred.objects = Mock(filter=a_filter)

        mock_command = Mock(spec=Command, add_report_data=Command().add_report_data)
        type(mock_command).groups_list = PropertyMock(return_value=group_list)

        expected_data = {QuestionsMovedToPublicDomainReport.report_label: 5}
        actual_data = Command.get_report_context_data(mock_command, group_list[0])['group_reports']['ABC'][tag_name]

        self.assertDictContainsSubset(expected_data, actual_data)

    @patch('knowledgepoint.management.commands.send_monthly_admin_report.QuestionsAnsweredFromOtherSitesReport.model')
    def test_get_report_context_contains_numbers_of_question_answered_from_public_domain(self, questions_transferred):
        group_list = [N(Group, name='ABC')]
        tag_name = format_report_tag(
            ANSWERED_QUESTIONS_FROM_PUBLIC_DOMAIN_NAME,
            group_list[0]
        )

        def get_filter(count):
            return Mock(return_value=Mock(count=lambda: count))

        answered_posts = Mock(filter=get_filter(5))
        unanswered_posts = Mock(filter=get_filter(0))

        mock_threads = [
            Mock(posts=answered_posts),
            Mock(posts=unanswered_posts)
        ]

        mock_activities = []

        for thread in mock_threads:
            question = Mock(thread=thread)
            activity = Mock(question=question)
            type(activity.content_object).name = 'ABC'
            mock_activities.append(activity)

        a_filter = Mock(return_value=mock_activities)

        questions_transferred.objects = Mock(filter=a_filter)

        mock_command = Mock(spec=Command, add_report_data=Command().add_report_data)
        type(mock_command).groups_list = PropertyMock(return_value=group_list)

        expected_data = {QuestionsAnsweredFromOtherSitesReport.report_label: 5}

        actual_data = Command.get_report_context_data(mock_command, group_list[0])['group_reports']['ABC'][tag_name]

        self.assertDictContainsSubset(expected_data, actual_data)
