from django.contrib.auth.models import Group, User
from django.contrib.contenttypes.models import ContentType
from django.contrib.sites.models import Site
from django.test import TestCase

from askbot.const import TYPE_ACTIVITY_POST_SHARED_WITH_SPACE
from askbot.deps.livesettings.models import LongSetting, Setting
from askbot.models.post import Post
from askbot.models.question import ThreadToGroup, Thread
from askbot.models.spaces import Space
from askbot.models.tag import Tag, MarkedTag
from askbot.models.user import (
    Activity,
    GroupMembership,
    AuthUserGroups,
    Group as AskbotGroup
)
from django_dynamic_fixture import G, F, N
from mock import Mock, patch

from ..reports import (
    AnswersAndCommentsByGroupReport,
    BaseReport,
    GroupMembershipReport,
    MembersInCountryReport,
    MembersInterestedInAreaReport,
    QuestionsAnsweredFromOtherSitesReport,
    QuestionsAskedByGroupReport,
    QuestionsGroupedByCountryReport,
    QuestionsGroupedByTagReport,
    QuestionsMovedToPublicDomainReport,
    QuestionsMovedFromOtherSitesReport,
    UnansweredQuestionsByGroupReport,
)


class ReportTest(TestCase):
    def add_settings_limiting_tags(self):
        site = Site.objects.get_current()
        G(
            Setting,
            site=site,
            group='FORUM_DATA_RULES',
            key='TAG_SOURCE',
            value='category-tree'
        )
        G(
            LongSetting,
            site=site,
            group='FLATPAGES',
            key='CATEGORY_TREE_EN',
            value='[["dummy", [["Agriculture", []], ["DRR", []], ["Energy", []], ["Other", []], ["WASH", []]]]]'
        )

    def test_report_has_method_to_get_data(self):
        report = BaseReport('')
        self.assertTrue(hasattr(report, 'generate_data'))

    def test_report_uses_model(self):
        report = BaseReport('')
        self.assertTrue(hasattr(report, 'model'))

    def test_method_must_be_overridden(self):
        report = BaseReport('')
        self.assertRaises(NotImplementedError, report.generate_data)

    def test_group_membership_count_uses_group_membership_model(self):
        report = GroupMembershipReport(group_name='ABC')
        self.assertEqual(GroupMembership, report.model)

    def test_group_membership_report_takes_group_id(self):
        report = GroupMembershipReport(group_name='ABC')
        self.assertTrue(hasattr(report, 'group_name'))

    @patch('knowledgepoint.reports.GroupMembership.objects')
    def test_group_membership_report_generates_number_of_group_members(self, manager):
        report = GroupMembershipReport(group_name='ABC')
        manager.filter = Mock(
            return_value=Mock(exclude=Mock(return_value=Mock(count=lambda: 2)))
        )

        expected_data = {report.report_label: 2}
        actual_data = report.generate_data()
        self.assertDictEqual(expected_data, actual_data)

    def test_filter_for_group_membership_returns_correct_results(self):
        # The criteria used to test for group membership is taken from the
        # group page view in Askbot.
        GROUP_NAME = 'Pi Caname'
        group = G(Group, name=GROUP_NAME)
        G(GroupMembership, group=group, level=GroupMembership.FULL, n=5)
        banned_user = G(User, status='b')
        G(
             GroupMembership,
             group=group,
             level=GroupMembership.FULL,
             user=banned_user
        )

        report = GroupMembershipReport(group_name=GROUP_NAME)
        expected_data = {report.report_label: 5}

        actual_data = report.generate_data()
        self.assertDictEqual(expected_data, actual_data)

    def test_group_membership_report_creation_takes_group_name(self):
        GroupMembershipReport(group_name='ABC')
        self.assertTrue(True)

    def test_group_membership_report_creation_requires_group_name(self):
        self.assertRaises(TypeError, GroupMembershipReport)

    @patch('knowledgepoint.reports.MarkedTag.objects')
    def test_intersting_area_report_gets_users_interested_in_tags(self, manager):
        TAG_NAME = 'WASH'
        report = MembersInterestedInAreaReport('ABC')

        def annotate(instance, **_):
            return [{'tag__name': TAG_NAME, 'tag__name__count': 3}]

        values = Mock(return_value=Mock(annotate=annotate))
        exclude = Mock(return_value=Mock(values=values))
        manager.filter = Mock(
            return_value=Mock(exclude=exclude)
        )

        expected_data = {TAG_NAME: 3}
        actual_data = report.generate_data()
        self.assertDictEqual(expected_data, actual_data)

    def test_filter_for_interest_in_group_returns_correct_results(self):
        GROUP_NAME = 'Pi Caname'
        TAG_1_NAME = 'Agriculture'
        TAG_2_NAME = 'Waste'

        self.add_settings_limiting_tags()

        group = G(Group, name=GROUP_NAME)
        authusergroups_list = G(AuthUserGroups, group=group, n=5)
        for authusergroup in authusergroups_list:
            G(
                GroupMembership,
                authusergroups_ptr=authusergroup,
                group=authusergroup.group,
                user=authusergroup.user,
                level=GroupMembership.FULL,
            )

        expected_data = {TAG_1_NAME: 3}
        banned_user = G(User, status='b')

        authusergroup = G(AuthUserGroups, group=group, user=banned_user)
        G(
             GroupMembership,
             authusergroups_ptr=authusergroup,
             group=authusergroup.group,
             user=authusergroup.user,
             level=GroupMembership.FULL,
        )

        tag1 = G(
            Tag,
            created_by=banned_user,
            deleted_by=banned_user,
            tag_wiki=None,
            name=TAG_1_NAME
        )

        tag2 = G(
            Tag,
            created_by=banned_user,
            deleted_by=banned_user,
            tag_wiki=None,
            name=TAG_2_NAME
        )

        tag = tag1
        users_list = User.objects.all()
        for user in users_list:
            G(MarkedTag, tag=tag, user=user, reason=MarkedTag.TAG_MARK_REASONS[0][0])
            # Alternate between tags
            tag = tag2 if tag == tag1 else tag1

        report = MembersInterestedInAreaReport(group_name=GROUP_NAME)

        actual_data = report.generate_data()
        self.assertDictEqual(expected_data, actual_data)

    @patch('knowledgepoint.reports.GroupMembership.objects')
    def test_get_member_numbers_for_country_offices(self, manager):
        report = MembersInCountryReport('ABC')

        def annotate(instance, **_):
            return [{'user__country': 'GB', 'user__country__count': 3}]

        values = Mock(return_value=Mock(annotate=annotate))
        exclude = Mock(return_value=Mock(values=values))

        manager.filter = Mock(return_value=Mock(exclude=exclude))

        expected_data = {'United Kingdom': 3}
        actual_data = report.generate_data()
        self.assertDictEqual(expected_data, actual_data)

    def test_filter_for_group_country_membership_returns_correct_results(self):
        GROUP_NAME = 'Pi Caname'
        group = G(Group, name=GROUP_NAME)
        G(
            GroupMembership,
            group=group,
            user=F(country='AF'),
            level=GroupMembership.FULL,
            n=3,
        )
        G(
            GroupMembership,
            group=group,
            user=F(country='GB'),
            level=GroupMembership.FULL,
            n=2,
        )
        expected_data = {'Afghanistan': 3, 'United Kingdom': 2}
        banned_user = G(User, status='b')
        G(
             GroupMembership,
             group=group,
             level=GroupMembership.FULL,
             user=banned_user
        )

        report = MembersInCountryReport(group_name=GROUP_NAME)

        actual_data = report.generate_data()
        self.assertDictEqual(expected_data, actual_data)

    @patch('knowledgepoint.reports.ThreadToGroup.objects')
    def test_report_gets_number_of_questions_asked_by_group(self, manager):
        report = QuestionsAskedByGroupReport('ABC')
        manager.filter = Mock(
            return_value=Mock(count=lambda: 4)
        )

        expected_data = {report.report_label: 4}
        actual_data = report.generate_data()
        self.assertDictEqual(expected_data, actual_data)

    def test_report_questions_for_group_reports_query_returns_correct_data(self):
        group = G(AskbotGroup, name='My Group')
        G(ThreadToGroup, group=group, n=5)
        G(ThreadToGroup)
        report = QuestionsAskedByGroupReport('My Group')
        expected_data = {report.report_label: 5}
        actual_data = report.generate_data()

        self.assertDictEqual(expected_data, actual_data)

    @patch('knowledgepoint.reports.Group.objects')
    def test_questions_asked_by_thread_report_maps_tags_onto_question_numbers(self, groups):

        def annotate(_):
            return [{
                'group_threads__tags__name__count': 5,
                'group_threads__tags__name': 'Agriculture'
            }, {
                'group_threads__tags__name__count': 3,
                'group_threads__tags__name': 'Waste'
            }]

        values = Mock(return_value=Mock(annotate=annotate))
        groups.filter = Mock(return_value=Mock(values=values))

        expected_data = {'Agriculture': 5}

        report = QuestionsGroupedByTagReport('Pi Caname')
        actual_data = report.generate_data()

        self.assertDictEqual(expected_data, actual_data)

    def test_questions_asked_by_thread_query_returns_correct_data(self):
        self.add_settings_limiting_tags()

        water_tag = G(Tag, name='Water')
        agriculture_tag = G(Tag, name='Agriculture')
        waste_tag = G(Tag, name='WASH')
        tags_set_1 = [agriculture_tag, waste_tag]
        tags_set_2 = [water_tag, waste_tag]
        tags_set_3 = [water_tag, agriculture_tag]
        groups = [G(AskbotGroup, name='Pi Caname')]

        G(AskbotGroup)
        G(Thread, groups=groups, tags=tags_set_1)
        G(Thread, groups=groups, tags=tags_set_2)
        G(Thread, groups=groups, tags=tags_set_3)
        G(Thread, tags=tags_set_1)

        expected_data = {'Agriculture': 2, 'WASH': 2}

        report = QuestionsGroupedByTagReport('Pi Caname')
        actual_data = report.generate_data()

        self.assertDictEqual(expected_data, actual_data)

    @patch('knowledgepoint.reports.Group.objects')
    def test_questions_asked_by_country_report_maps_threads_onto_questions_country(self, groups):

        def annotate(_):
            return [{'author__country': 'GB', 'author__country__count': 1}]

        values = Mock(annotate=annotate)
        the_filter = Mock(values=Mock(return_value=values))
        posts = Mock(filter=Mock(return_value=the_filter))
        group_threads = Mock(all=Mock(return_value=[Mock(posts=posts)]))
        groups.get = lambda **_: Mock(group_threads=group_threads)

        expected_data = {'United Kingdom': 1}

        report = QuestionsGroupedByCountryReport('Pi Caname')
        actual_data = report.generate_data()

        self.assertDictEqual(expected_data, actual_data)

    @patch('knowledgepoint.reports.Post.objects')
    def test_answers_and_comments_report_gets_numbers_of_answers_and_comments(self, posts):
        def get_post_type_filter(**kwargs):
            if kwargs.get('post_type', '') == 'answer':
                the_filter = Mock(count=lambda: 7)
            elif kwargs.get('post_type', '') == 'comment':
                the_filter = Mock(count=lambda: 3)
            else:
                the_filter = Mock(count=lambda: 10)
            return the_filter

        posts.filter = Mock(return_value=Mock(filter=get_post_type_filter))

        report = AnswersAndCommentsByGroupReport('Pi Caname')
        expected_data = {
            'Number of answers': 7,
            'Number of comments': 3,
            'Number of answers and comments': 10
        }
        actual_data = report.generate_data()

        self.assertDictEqual(expected_data, actual_data)

    def test_answers_and_comments_report_returns_correct_data(self):
        group = G(Group, name="Pi Caname")
        group_membership = G(AuthUserGroups, group=group)
        user = group_membership.user
        thread = G(Thread, accepted_answer=None)

        question = G(Post, author=user, thread=thread, post_type='question', parent=None)
        G(Post, author=user, thread=thread, post_type='answer', n=2, parent=question)
        G(Post, author=user, thread=thread, post_type='comment', n=3, parent=question)
        G(Post)

        report = AnswersAndCommentsByGroupReport('Pi Caname')
        expected_data = {
            'Number of answers': 2,
            'Number of comments': 3,
            'Number of answers and comments': 5
        }
        actual_data = report.generate_data()

        self.assertDictEqual(expected_data, actual_data)

    @patch('knowledgepoint.reports.ThreadToGroup.objects')
    def test_unanswered_question_report_returns_number_of_unanswered_questions(self, threads):
        exclude = Mock(return_value=Mock(count=lambda: 5))

        threads.filter = Mock(return_value=Mock(exclude=exclude))

        report = UnansweredQuestionsByGroupReport('Pi Caname')
        expected_data = {report.report_label: 5}
        actual_data = report.generate_data()

        self.assertDictEqual(expected_data, actual_data)

    def test_unanswered_question_report_returns_correct_data(self):
        group = G(AskbotGroup, name="Pi Caname")
        user = G(User)
        thread_groups = G(ThreadToGroup, group=group, accepted_answer=None, n=2)

        question = G(Post, author=user, thread=thread_groups[0].thread, post_type='question', parent=None)
        G(Post, author=user, thread=thread_groups[0].thread, post_type='answer', parent=question)
        for the_group in thread_groups:
            G(Post, author=user, thread=the_group.thread, post_type='question', parent=None)

        report = UnansweredQuestionsByGroupReport('Pi Caname')
        expected_data = {report.report_label: 1}
        actual_data = report.generate_data()

        self.assertDictEqual(expected_data, actual_data)

    @patch('knowledgepoint.reports.Thread.objects')
    def test_theads_moved_to_public_report_returns_number_of_public_threads(self, threads):
        second_filter = Mock(return_value=Mock(count=lambda: 5))
        first_filter = Mock(return_value=Mock(filter=second_filter))
        threads.filter = first_filter

        report = QuestionsMovedToPublicDomainReport('Pi Caname')
        expected_data = {report.report_label: 5}
        actual_data = report.generate_data()
        self.assertDictEqual(expected_data, actual_data)

    def test_threads_moved_to_public_report_returns_correct_data(self):
        global_group = AskbotGroup.objects.get_global_group()
        group = G(AskbotGroup, name="Pi Caname")

        G(Thread, groups=[group, global_group], accepted_answer=None, n=5)
        G(Thread, groups=[global_group], accepted_answer=None)
        G(Thread, groups=[group], accepted_answer=None)

        report = QuestionsMovedToPublicDomainReport('Pi Caname')
        expected_data = {report.report_label: 5}
        actual_data = report.generate_data()

        self.assertDictEqual(expected_data, actual_data)

    @patch('knowledgepoint.reports.Activity.objects')
    def test_threads_moved_from_public_report_returns_number_of_questions_shared_with_space(self, activities):
        content_type = ContentType.objects.get_for_model(Space)
        an_object = N(Space, name='Pi Caname', id=1)

        activity_list = N(
            Activity,
            activity_type=TYPE_ACTIVITY_POST_SHARED_WITH_SPACE,
            content_type=content_type,
            object_id=an_object.id,
            persist_dependencies=False,
            n=5
        )

        # Actual IDs start at one, so the mock ones should as well
        for index, activity in enumerate(activity_list, 1):
            activity.content_object = an_object
            activity.question_id = index

        activities.filter = lambda **_: activity_list

        report = QuestionsMovedFromOtherSitesReport('Pi Caname')
        expected_data = {report.report_label: 5}

        actual_data = report.generate_data()
        self.assertDictEqual(expected_data, actual_data)

    def test_threads_moved_from_public_report_returns_correct_data(self):
        content_type = ContentType.objects.get_for_model(Space)
        an_object = G(Space, name='Pi Caname')

        G(
            Activity,
            activity_type=TYPE_ACTIVITY_POST_SHARED_WITH_SPACE,
            content_type=content_type,
            object_id=an_object.id,
            n=5
        )

        report = QuestionsMovedFromOtherSitesReport('Pi Caname')
        expected_data = {report.report_label: 5}

        actual_data = report.generate_data()
        self.assertDictEqual(expected_data, actual_data)

    @patch('knowledgepoint.reports.Activity.objects')
    def test_threads_with_answers_moved_from_public_report_returns_count_of_questions_moved_from_public_with_answers(self, activities):
        an_object = N(Space, name='Pi Caname')

        thread = Mock()
        thread.posts.filter = Mock(return_value=Mock(count=lambda: 5))

        activity_list = []
        for _ in range(5):
            activity_list.append(Mock(
                question=Mock(thread=thread),
                activity_type=TYPE_ACTIVITY_POST_SHARED_WITH_SPACE,
                content_object=an_object
            ))

        second_thread = Mock()
        second_thread.posts.filter = Mock(return_value=Mock(count=lambda: 0))
        for _ in range(5):
            activity_list.append(Mock(
                question=Mock(thread=second_thread),
                activity_type=TYPE_ACTIVITY_POST_SHARED_WITH_SPACE,
                content_object=an_object
            ))

        activities.filter = lambda **_: activity_list

        report = QuestionsAnsweredFromOtherSitesReport('Pi Caname')
        expected_data = {report.report_label: 5}

        actual_data = report.generate_data()
        self.assertDictEqual(expected_data, actual_data)

    def test_threads_answered_from_public_report_returns_correct_data(self):
        content_type = ContentType.objects.get_for_model(Space)
        an_object = G(Space, name='Pi Caname')

        thread_list = G(Thread, n=5)

        for thread in thread_list:
            question = G(Post, thread=thread, post_type='question')
            G(Post, thread=thread, post_type='answer', parent=question)

        unanswered_thread_list = G(Thread, n=5)

        for thread in unanswered_thread_list:
            G(Post, thread=thread, post_type='question', parent=None)

        for post in Post.objects.filter(thread_id__isnull=False):
            G(
                Activity,
                activity_type=TYPE_ACTIVITY_POST_SHARED_WITH_SPACE,
                content_type=content_type,
                object_id=an_object.id,
                question=post
            )

        report = QuestionsAnsweredFromOtherSitesReport('Pi Caname')
        expected_data = {report.report_label: 5}

        actual_data = report.generate_data()
        self.assertDictEqual(expected_data, actual_data)
