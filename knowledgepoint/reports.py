from collections import OrderedDict
from django.db.models.aggregates import Count

from askbot.const import TYPE_ACTIVITY_POST_SHARED_WITH_SPACE
from askbot.models.post import Post
from askbot.models.question import Thread, ThreadToGroup
from askbot.models.tag import MarkedTag
from askbot.models.user import Activity, GroupMembership, Group
from askbot.utils.category_tree import get_data as get_category_tree_data

from django_countries import countries


class BaseReport(object):
    model = None
    group_name = None
    report_label = ''

    def __init__(self, group_name):
        self.group_name = group_name

    def generate_data(self):
        raise NotImplementedError


class TagGroupedReport(BaseReport):
    @property
    def categories(self):
        category_list = get_category_tree_data()[0][1]
        return [category[0] for category in category_list]


class GroupMembershipReport(BaseReport):
    model = GroupMembership
    report_label = 'Number of members:'

    def generate_data(self):
        membership_numbers = self.model.objects.filter(
            group__name=self.group_name, level=GroupMembership.FULL
        ).exclude(user__status='b').count()
        return {self.report_label: membership_numbers}


class MembersInterestedInAreaReport(TagGroupedReport):
    model = MarkedTag

    def generate_data(self):
        number_members_interested = self.model.objects.filter(
            reason='good',
            user__authusergroups__group__name=self.group_name
        ).exclude(user__status='b').values('tag__name').annotate(Count('tag__name'))

        report_data = {}
        for item in number_members_interested:
            if item['tag__name'] in self.categories:
                report_data[item['tag__name']] = item['tag__name__count']

        return report_data


class MembersInCountryReport(BaseReport):
    model = GroupMembership

    def generate_data(self):
        country_membership_numbers = self.model.objects.filter(
            group__name=self.group_name,
            level=GroupMembership.FULL,
        ).exclude(user__status='b').values('user__country').annotate(Count('user__country'))

        report_data = {}
        for item in country_membership_numbers:
            country_name = countries.name(item['user__country']).decode('UTF-8')
            if not country_name:
                country_name = 'Total unspecified'
            report_data[country_name] = item['user__country__count']

        return report_data


class QuestionsAskedByGroupReport(BaseReport):
    model = ThreadToGroup
    report_label = 'Number of questions:'

    def generate_data(self):
        question_numbers = self.model.objects.filter(
            group__name=self.group_name,
        ).count()
        return {self.report_label: question_numbers}


class QuestionsGroupedByTagReport(TagGroupedReport):
    model = Group

    def generate_data(self):
        number_of_questions_tagged = self.model.objects.filter(
            name=self.group_name
        ).values('group_threads__tags__name').annotate(Count('group_threads__tags__name'))

        report_data = {}
        for item in number_of_questions_tagged:
            tag_name = item['group_threads__tags__name']
            if tag_name in self.categories:
                report_data[tag_name] = item['group_threads__tags__name__count']

        return report_data


class QuestionsGroupedByCountryReport(BaseReport):
    model = Group

    def generate_data(self):
        try:
            group = self.model.objects.get(name=self.group_name)
        except Group.DoesNotExist:
            # This is only an issue in tests, but it still needs dealing with
            return {}

        thread_countries = {}
        for thread in group.group_threads.all():
            thread_data = thread.posts.filter(post_type='question').values('author__country').annotate(Count('author__country'))
            for item in thread_data:
                country_name = countries.name(item['author__country']).decode('UTF-8')
                if not country_name:
                    country_name = 'Total unspecified'
                if country_name in thread_countries:
                    thread_countries[country_name] += item['author__country__count']
                else:
                    thread_countries[country_name] = item['author__country__count']
        return thread_countries


class AnswersAndCommentsByGroupReport(BaseReport):
    model = Post

    def generate_data(self):
        number_of_answers = self.model.objects.filter(
            author__authusergroups__group__name=self.group_name
        ).filter(post_type='answer').count()
        number_of_comments = self.model.objects.filter(
            author__authusergroups__group__name=self.group_name
        ).filter(post_type='comment').count()
        number_of_answers_and_comments = self.model.objects.filter(
            author__authusergroups__group__name=self.group_name
        ).filter(post_type__in=['answer', 'comment']).count()
        results = OrderedDict()
        results['Number of answers'] = number_of_answers
        results['Number of comments'] = number_of_comments
        results['Number of answers and comments'] = number_of_answers_and_comments
        return results


class UnansweredQuestionsByGroupReport(BaseReport):
    model = ThreadToGroup
    report_label = 'Number of questions with no answer:'

    def generate_data(self):
        number_of_unanswered_questions = self.model.objects.filter(
            group__name=self.group_name
        ).exclude(
            thread__posts__post_type='answer'
        ).count()

        return {self.report_label: number_of_unanswered_questions}


class QuestionsMovedToPublicDomainReport(BaseReport):
    model = Thread
    report_label = 'Number of threads moved to public domain:'

    def generate_data(self):
        global_group = Group.objects.get_global_group()
        threads_moved_to_public_domain_count = self.model.objects.filter(
            groups__name=self.group_name
        ).filter(groups__name=global_group.name).count()

        return {self.report_label: threads_moved_to_public_domain_count}


class QuestionsMovedFromOtherSitesReport(BaseReport):
    model = Activity
    report_label = 'Number of questions moved from public domain:'

    def generate_data(self):
        questions = set([
            activity.question_id for activity in self.model.objects.filter(activity_type=TYPE_ACTIVITY_POST_SHARED_WITH_SPACE)
            if activity.content_object.name == self.group_name
        ])
        return {self.report_label: len(questions)}


class QuestionsAnsweredFromOtherSitesReport(BaseReport):
    model = Activity
    report_label = 'Number of questions answered from public domain:'

    def generate_data(self):
        thread_list = set([
            activity.question.thread for activity in self.model.objects.filter(activity_type=TYPE_ACTIVITY_POST_SHARED_WITH_SPACE)
            if activity.content_object.name == self.group_name
        ])

        answered_questions = 0
        for thread in thread_list:
            answered_questions += thread.posts.filter(post_type='answer').count()

        return {self.report_label: answered_questions}
